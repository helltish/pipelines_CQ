nextflow.enable.dsl = 2
  
//  container "https://depot.galaxyproject.org/singularity/"

params.roche = false

process prefetch {
  storeDir "${params.outdir}"
  container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}/${accession}.sra"
  script:
    """
    prefetch $accession
    """
}

process fastqdump {
  storeDir "${params.outdir}"
  container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
  input:
    path input_sra
    val accession
  output:
    path "${accession}_?.fastq", emit: fastqfiles
  script:
    """
    fastq-dump "${input_sra}" --split-files
    """
}

process utils_stats {
  publishDir "${params.outdir}", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/ngsutils%3A0.5.9--py27heb79e2c_4"
  input:
    path fastq
  output:
    path "${fastq}.stats"
  script:
    """
    fastqutils stats ${fastq} > ${fastq}.stats
    """
}

process fastqc {
  publishDir "${params.outdir}", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--0"
  input:
    path fastq
  output:
    path "${fastq.getSimpleName()}_fastqc.html"
    path "${fastq.getSimpleName()}_fastqc.zip", emit: zipped
  script:
    """
    fastqc ${fastq}
    """
}


process multiqc { 
  publishDir "${params.outdir}", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/multiqc:1.9--py_1"
  input:
    path input_folder
    path output_fastqc // dummy value to force this process to wait for fastqc output
    path output_fastp // dummy value to force this process to wait for fastp output
  output:
    path "${params.accession}_multiqc"
  script:
    """
    multiqc ${input_folder} --outdir ${params.accession}_multiqc
    """

}

process fastp {
  publishDir "${params.outdir}", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastp:0.20.1--h8b12597_0"
  input:
    path fastq
  output:
    path "*fastp.fastq", emit: fastqfiles
    path "*report_fastp.*", emit: fastpreport
  script:
    if (fastq instanceof List) {
    """
    fastp -i ${fastq[0]} -I ${fastq[1]} -o ${params.accession}_1_fastp.fastq -O ${params.accession}_2_fastp.fastq -h ${params.accession}_report_fastp.html -j ${params.accession}_report_fastp.json
    """
    } else {
    """
    fastp -i ${fastq} -o ${fastq.getSimpleName()}_fastp.fastq -h ${fastq.getSimpleName()}_fastp_report.html -j ${fastq.getSimpleName()}_report_fastp.json
      """
    }
}


workflow {

    sraresult = prefetch(params.accession)
    fastqs = fastqdump(sraresult, params.accession)
  
    if(params.roche){
      fastqs = fastqs.flatten().last()
    }
    
    trimmed = fastp(fastqs)
    all_fastqs = fastqs.concat(trimmed.fastqfiles)
    qcied = fastqc(all_fastqs.flatten())
    input_channel = Channel.fromPath(params.outdir)
    multiqc(input_channel, qcied.zip.last(), trimmed.fastpreport)   

}
