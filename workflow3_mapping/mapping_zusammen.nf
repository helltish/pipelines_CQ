nextflow.enable.dsl = 2

params.indexdir = null
params.outdir = null
params.reference = null
params.indir = null

process build_index {
  storeDir "${params.indexdir}" // wird nicht in dem outdir gespeichert, sondern in einem externen indexdir
  container "https://depot.galaxyproject.org/singularity/bowtie2:2.4.4--py39hbb4e92a_0"
  input:
    path input_fasta
  output:
    path "${input_fasta.getSimpleName()}"
  script:
  // erstellt einen ordner mit dem Namen von fasta Datei, und drin 6 Dateien mit dem gleichen Namen wie fasta Datei
  """
  mkdir ${input_fasta.getSimpleName()}
  bowtie2-build ${input_fasta} ${input_fasta.getSimpleName()}/${input_fasta.getSimpleName()}  
  """  
}

process mapping_bowtie2{
  //storeDir "${params.outdir}"
  publishDir "${params.outdir}", mode: 'copy', overwrite: true
  container "https://depot.galaxyproject.org/singularity/bowtie2:2.4.4--py39hbb4e92a_0"
  input:
    path input_fastq // [fastq1 fastq2]   input_fastq[0]
    path index_directory
    path fasta
  output:
    path "mapping.sam"
  script:
  // mach eine if Abfrage ob input Datei eine oder zwei fastqs beinhaltet
  if(input_fastq instanceof List){
  """
  bowtie2 -x ${index_directory}/${fasta.getSimpleName()} -1 ${input_fastq[0]} -2 ${input_fastq[1]} -S mapping.sam
  """
  } else {
  """
  bowtie2 -x ${index_directory}/${fasta.getSimpleName()} -U ${input_fastq} -S mapping.sam
  """
  }
  
}


process sorting_sam {
  publishDir "${params.outdir}", mode: 'copy', overwrite: true 
  container "https://depot.galaxyproject.org/singularity/samtools:1.9--h91753b0_8"
  input:
    path samfile
  output:
    path "sorted_mapping.sam"
  script:
  """
  samtools sort -O sam ${samfile} -o sorted_mapping.sam
  """
}


process variant_call {
  publishDir "${params.outdir}", mode: 'copy', overwrite: true
  container "https://depot.galaxyproject.org/singularity/bcftools:1.9--ha228f0b_4"
  input:
    path infasta
    path samfile
  output:
    path "snps.vcf"
  script:
  """
  bcftools mpileup -Ou -f ${infasta} ${samfile} | bcftools call -mv -Ov --ploidy 1 -o snps.vcf
  """
}


workflow {
  infasta = Channel.fromPath(params.reference)
  infastq = Channel.fromPath("${params.indir}/*.fastq").collect().sort() // in diesem Kanal sollen 1 oder 2 fatsq Dateien landen
  // .collect() macht aus fastq1, fastq2 -> [fastq1, fastq2]
  indexdir = build_index(infasta) // indexdir ist das output von dem Prozess build_index(), in unserem Fall ist es nur ein Kanal, manchmal sind mehrere Kanäle ausgegeben, da muss man aufpassen
  samfile = mapping_bowtie2(infastq, indexdir, infasta) // nimmt reads und index, gibt samfile aus eine Datei in einem Kanal
  // mapping_bowtie() Prozess nimmt entweder eine fastq-Datei (single-end sequencing) oder [XXX_1.fastq, XXX_2.fastq] fuer paired-end sequencing
  sorted_samfile = sorting_sam(samfile)
  variant_call(infasta, sorted_samfile)

}
